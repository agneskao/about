import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import Home from '@/layouts/Home.vue'
import About from '@/pages/About.vue'
import Showcase from '@/pages/Showcase.vue'
import Frontend from '@/pages/Frontend/index.vue'
import FrontendIntro from '@/pages/Frontend/Frontend.vue'
import FrontendInfo from '@/pages/Frontend/FrontendInfo.vue'
import Website from '@/pages/Website.vue'
import Design from '@/pages/Design.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      title: {
        zh: '高慈謙',
        en: 'Agnes Kao',
      },
    },
    children: [
      {
        path: 'about',
        name: 'about',
        component: About,
        meta: {
          title: '關於我 About',
        }
      },
      {
        path: 'showcase',
        name: 'showcase',
        component: Showcase,
        meta: {
          title: '作品 Showcase'
        },
        redirect: {
          name: 'frontendProject'
        },
        children: [
          {
            path: 'frontend-project',
            component: Frontend,
            name: 'frontendProject',
            meta: {
              title: '前端專案'
            },
            redirect: {
              name: 'frontendIntro'
            },
            children: [
              {
                path: '',
                name: 'frontendIntro',
                component: FrontendIntro,
              },
              {
                path: ':name/:tag',
                name: 'frontendInfo',
                component: FrontendInfo,
              },
            ]
          },
          // {
          //   path: 'website-design',
          //   name: 'websiteDesign',
          //   component: Website,
          //   meta: {
          //     title: '網頁設計',
          //     hide: import.meta.env.MODE !== 'development'
          //   }
          // },
          // {
          //   path: 'graphic-design',
          //   name: 'graphicDesign',
          //   component: Design,
          //   meta: {
          //     title: '平面設計',
          //     hide: import.meta.env.MODE !== 'development'
          //   }
          // }
        ]
      },

    ]
  },
  {
    path: '/:catchAll(.*)',
    redirect: {name: 'home'},
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior: () => {
    return { left: 0, top: 0 }
  },
})
router.beforeEach((to) => {
  const body = document.getElementsByTagName('body')[0];
  if (to.name !== 'home') return body.classList.add('home')
  body.classList.remove('home')
})

export default router
